from django.contrib import admin
from main.models import *
from sorl.thumbnail.admin import AdminImageMixin
# Register your models here.


@admin.register(MenuItem)
class AdminMenuItem(admin.ModelAdmin):
    list_display = ['title', 'weight']
    ordering = ('-weight',)


@admin.register(Category)
class AdminCategory(admin.ModelAdmin):
    list_display = ['title']
    ordering = ('-id',)
    prepopulated_fields = {'slug': ('title',)}



@admin.register(Post)
class AdminPost(AdminImageMixin, admin.ModelAdmin):
    list_display = ['title', 'category', 'preview', 'author', 'date', 'published']
    search_fields = ('title',)
    list_filter = ('category', 'author', 'published', 'date')
    ordering = ('-date',)
    list_per_page = 20
    date_hierarchy = 'date'
    prepopulated_fields = {'slug': ('title',)}


@admin.register(PortfolioCategory)
class AdminPortfolioCategory(admin.ModelAdmin):
    list_display = ['title']
    ordering = ('-id',)
    prepopulated_fields = {'slug': ('title',)}


class AdminPortfolioImageInline(AdminImageMixin, admin.TabularInline):
    fieldsets = (
        (
            None,
            {
                'fields': ('image', 'title')
            }
        ),
    )
    model = PortfolioImage
    extra = 0


@admin.register(Portfolio)
class AdminPortfolio(AdminImageMixin, admin.ModelAdmin):
    list_display = ['title', 'category', 'preview', 'url']
    search_fields = ('title',)
    list_filter = ('category', 'date')
    ordering = ('-date',)
    inlines = (AdminPortfolioImageInline, )
    list_per_page = 20
    date_hierarchy = 'date'
    prepopulated_fields = {'slug': ('title',)}


@admin.register(StaticPage)
class AdminStaticPage(admin.ModelAdmin):
    list_display = ['title', ]
    ordering = ('-id',)


@admin.register(Testimonials)
class AdminTestimonials(admin.ModelAdmin):
    list_display = ['message', 'author']
    search_fields = ('message',)
    list_filter = ('date',)
    ordering = ('-date',)


@admin.register(Team)
class AdminTeam(AdminImageMixin, admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'preview', 'position']
    search_fields = ('first_name', 'last_name')
    ordering = ('-id',)


@admin.register(FAQ)
class AdminFAQ(admin.ModelAdmin):
    list_display = ['question', 'answer', 'is_active']
    ordering = ('-id',)


@admin.register(Subscriber)
class AdminSubscriber(admin.ModelAdmin):
    list_display = ['email', 'date', 'is_active']
    search_fields = ('email', 'date')
    ordering = ('email',)


@admin.register(Feedback)
class AdminFeedback(admin.ModelAdmin):
    list_display = ['email', 'name', 'phone', 'subject', 'date']
    search_fields = ('name', 'subject')
    list_filter = ('date',)
    date_hierarchy = 'date'
    ordering = ('-date',)