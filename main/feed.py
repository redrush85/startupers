# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.utils.html import strip_tags
from main.models import Post
from django.utils.feedgenerator import Rss201rev2Feed, RssFeed


class ExtendedRSSFeed(Rss201rev2Feed):

    # def root_attributes(self):
    #     attrs = super(ExtendedRSSFeed, self).root_attributes()
    #     attrs['xmlns:content'] = 'http://purl.org/rss/1.0/modules/content/'
    #     return attrs

    def add_item_elements(self, handler, item):
        super(ExtendedRSSFeed, self).add_item_elements(handler, item)
        handler.startElement(u'image', {})
        handler.addQuickElement(u'url', item['image_url'])
        handler.endElement(u'image')


class LatestNewsFeed(Feed):
    feed_type = ExtendedRSSFeed
    title = "Startup Enterpreneurs"
    link = "http://startupreneurs.se"
    description = ""

    def items(self):
        return Post.objects.filter().order_by('-date')[:10]

    def item_extra_kwargs(self, item):
        return {'image_url': self.item_enclosure_url(item)}

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return strip_tags(item.content)

    def item_image(self, item):
        return item.image

    def item_pubdate(self, item):
        return item.date

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return item.get_absolute_url()

    item_enclosure_mime_type = "image/jpg"

    def item_enclosure_url(self, item):
        return 'http://startupreneurs.se/media/%s' % item.image

    def item_enclosure_length(self, item):
        return item.image.size