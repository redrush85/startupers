# -*- coding: utf-8 -*-

from django.db import models
from sorl.thumbnail import ImageField
from sorl.thumbnail import get_thumbnail
from django.contrib.auth.models import User
from django.conf import settings
from ckeditor.fields import RichTextField
# Create your models here.


class MenuItem(models.Model):
    title = models.CharField(max_length=255)
    url = models.URLField()
    weight = models.IntegerField(unique=True)

    class Meta:
        verbose_name = "Menu item"
        verbose_name_plural = "Menu items"


class Category(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class Post(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(Category)
    content = RichTextField()
    annotation = models.CharField(max_length=255, blank=True)
    image = ImageField(upload_to='uploads/news/')
    author = models.ForeignKey(User)
    published = models.BooleanField(default=True)
    slug = models.SlugField(unique=True)
    date = models.DateTimeField()

    def __unicode__(self):
        return self.title

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    @models.permalink
    def get_absolute_url(self):
        return ('view_post', (), {
            'category_slug': self.category.slug,
            'post_slug': self.slug,
        })


class PortfolioCategory(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Portfolio category"
        verbose_name_plural = "Portfolio categories"


class Portfolio(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(PortfolioCategory)
    content = RichTextField()
    image = ImageField(upload_to='uploads/portfolio/', blank=True, null=True)
    url = models.URLField(blank=True)
    slug = models.SlugField(unique=True)
    date = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True

    class Meta:
        verbose_name = "Portfolio item"
        verbose_name_plural = "Portfolio items"

    @models.permalink
    def get_absolute_url(self):
        return ('view_portfolio', (), {
            'slug': self.slug,
        })


class PortfolioImage(models.Model):
    image = ImageField(upload_to='uploads/portfolio/images/')
    title = models.CharField(max_length=255, blank=True)
    portfolio = models.ForeignKey(Portfolio)


class StaticPage(models.Model):
    title = models.CharField(max_length=200)
    content = RichTextField()
    slug = models.SlugField(unique=True)
    template = models.FilePathField(path=settings.TEMPLATES_MAIN_DIR+'/static/')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Static page"
        verbose_name_plural = "Static pages"


class Testimonials(models.Model):
    message = RichTextField()
    author = models.CharField(max_length=255)
    date = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.message

    class Meta:
        verbose_name = "Testimonial"
        verbose_name_plural = "Testimonials"


class Team(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    position = models.CharField(max_length=255)
    image = ImageField(upload_to='uploads/team/')
    about = RichTextField(blank=True)
    facebook_url = models.URLField(blank=True)
    twitter_url = models.URLField(blank=True)

    class Meta:
        verbose_name = "Team"
        verbose_name_plural = "Team"

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True


class FAQ(models.Model):
    question = models.CharField(max_length=255)
    answer = RichTextField()
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "FAQ"
        verbose_name_plural = "FAQ"


class Subscriber(models.Model):
    email = models.EmailField(max_length=75, unique=True)
    date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Subscriber"
        verbose_name_plural = "Subscribers"


class Feedback(models.Model):
    name = models.CharField(max_length=255, blank=True)
    email = models.EmailField(max_length=75)
    phone = models.CharField(max_length=40, blank=True)
    subject = models.CharField(max_length=255, blank=True)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Feedback"
        verbose_name_plural = "Feedback"