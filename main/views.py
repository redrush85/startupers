from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from main.forms import FeedbackForm
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from main.models import *


# Create your views here.
def home(request):

    posts = Post.objects.filter(published=True).order_by('-date')[:2]
    portfolio = Portfolio.objects.all().order_by('?')[:4]
    testimonials = Testimonials.objects.all().order_by('-date')[:6]

    return render_to_response("index.html", {"posts": posts, 'portfolio': portfolio, 'testimonials': testimonials}, context_instance=RequestContext(request))


def portfolio(request):
    categories = PortfolioCategory.objects.all()
    items = Portfolio.objects.all().order_by('id')
    return render_to_response("portfolio.html", {"categories": categories, 'items': items}, context_instance=RequestContext(request))


def view_portfolio(request, slug):
    item = get_object_or_404(Portfolio, slug=slug)
    return render_to_response("project-details.html", {'item': item}, context_instance=RequestContext(request))


def blog(request):
    posts = Post.objects.filter(published=True).order_by('-date')
    return render_to_response("blog.html", {'posts': posts}, context_instance=RequestContext(request))


def view_blog_category(request, category_slug):
    category = get_object_or_404(Category, slug=category_slug)
    posts = Post.objects.filter(published=True, category=category).order_by('-date')
    return render_to_response("blog_category.html", {'posts': posts, 'category': category}, context_instance=RequestContext(request))


def view_post(request, category_slug, post_slug):
    post = get_object_or_404(Post, slug=post_slug)
    return render_to_response("post.html", {'post': post}, context_instance=RequestContext(request))


def about(request):
    team = Team.objects.all()
    return render_to_response("about.html", {'team': team}, context_instance=RequestContext(request))


def contacts(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/thanks/')
        else:
            return render_to_response('contact.html', {'form': form}, context_instance=RequestContext(request))
    else:
        form = FeedbackForm()
        return render_to_response('contact.html', {'form': form}, context_instance=RequestContext(request))


def thanks(request):
    return render_to_response("thanks.html",  context_instance=RequestContext(request))


def faq_page(request):
    faq = FAQ.objects.filter(is_active=True)
    return render_to_response("faq.html", {'faq': faq}, context_instance=RequestContext(request))


def services(request):
    return render_to_response("services.html",  context_instance=RequestContext(request))


def ajax_subscribe(request):
    if request.method != 'POST':
        return HttpResponse('Bad request', status=500)

    email = request.POST.get('email', '')

    try:
        validate_email(email)
    except ValidationError:
        return HttpResponse('Invalid E-Mail', status=500)

    try:
        Subscriber(email=email).save()
        return HttpResponse('Thank you for subscription', status=200)
    except IntegrityError:
        return HttpResponse('You have already subscribed', status=500)
    except:
        return HttpResponse('Unknown error', status=500)