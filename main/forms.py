from django.forms import ModelForm
from main.models import Feedback
from django import forms


class FeedbackForm(ModelForm):

    class Meta:
        model = Feedback
        exclude = ['date']

        error_messages = {
            'name': {
                'required': ("The name field is required."),
            },

        }