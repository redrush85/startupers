# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20150408_2113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='facebook_url',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='team',
            name='twitter_url',
            field=models.URLField(blank=True),
        ),
    ]
