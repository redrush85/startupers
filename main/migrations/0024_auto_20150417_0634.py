# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0023_auto_20150414_0931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfolio',
            name='url',
            field=models.URLField(blank=True),
        ),
    ]
