# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_auto_20150409_2053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staticpage',
            name='template',
            field=models.FilePathField(path=b'D:\\JOB\\project\\startupers/templates/static/'),
        ),
    ]
