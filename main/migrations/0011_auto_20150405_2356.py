# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_testimonials'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testimonials',
            name='message',
            field=models.TextField(),
        ),
    ]
