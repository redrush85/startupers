# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0014_auto_20150408_1948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staticpage',
            name='template',
            field=models.FilePathField(path=b'D:\\JOB\\project\\startupers/templates/static/'),
        ),
    ]
