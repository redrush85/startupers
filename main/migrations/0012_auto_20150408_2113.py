# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_auto_20150405_2356'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('surname', models.CharField(max_length=255)),
                ('position', models.CharField(max_length=255)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=b'uploads/team/')),
                ('about', models.CharField(max_length=255, blank=True)),
                ('facebook_url', models.URLField()),
                ('twitter_url', models.URLField()),
            ],
            options={
                'verbose_name': 'Team',
                'verbose_name_plural': 'Team',
            },
        ),
        migrations.AlterField(
            model_name='staticpage',
            name='template',
            field=models.FilePathField(path=b'D:\\JOB\\project\\startupers/templates/static/'),
        ),
    ]
