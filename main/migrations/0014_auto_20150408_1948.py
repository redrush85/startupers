# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_auto_20150408_2145'),
    ]

    operations = [
        migrations.RenameField(
            model_name='team',
            old_name='name',
            new_name='first_name',
        ),
        migrations.RenameField(
            model_name='team',
            old_name='surname',
            new_name='last_name',
        ),
        migrations.AlterField(
            model_name='staticpage',
            name='template',
            field=models.FilePathField(path=b'/Users/redrush/Develope/python/startupers/startupers/templates/static/'),
        ),
        migrations.AlterField(
            model_name='team',
            name='about',
            field=models.TextField(blank=True),
        ),
    ]
