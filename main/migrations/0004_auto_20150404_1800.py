# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20150404_1748'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Category', 'verbose_name_plural': 'Categories'},
        ),
        migrations.AlterModelOptions(
            name='menuitem',
            options={'verbose_name': 'Menu item', 'verbose_name_plural': 'Menu items'},
        ),
        migrations.AlterModelOptions(
            name='news',
            options={'verbose_name': 'News', 'verbose_name_plural': 'News'},
        ),
        migrations.AlterModelOptions(
            name='portfolio',
            options={'verbose_name': 'Portfolio item', 'verbose_name_plural': 'Portfolio items'},
        ),
        migrations.AlterModelOptions(
            name='portfoliocategory',
            options={'verbose_name': 'Portfolio category', 'verbose_name_plural': 'Portfolio categories'},
        ),
        migrations.AlterModelOptions(
            name='staticpage',
            options={'verbose_name': 'Static page', 'verbose_name_plural': 'Static pages'},
        ),
    ]
