# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_auto_20150414_0237'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, blank=True)),
                ('email', models.EmailField(max_length=75)),
                ('phone', models.CharField(max_length=40, blank=True)),
                ('subject', models.CharField(max_length=255, blank=True)),
                ('message', models.TextField()),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Feedback',
                'verbose_name_plural': 'Feedback',
            },
        ),
        migrations.DeleteModel(
            name='Contacts',
        ),
        migrations.AlterField(
            model_name='staticpage',
            name='template',
            field=models.FilePathField(path=b'/Users/redrush/Develope/python/startupers/startupers/templates/static/'),
        ),
    ]
