# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20150404_1800'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='portfolio',
            name='screen',
        ),
        migrations.AddField(
            model_name='portfolio',
            name='date',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='portfolio',
            name='image',
            field=sorl.thumbnail.fields.ImageField(null=True, upload_to=b'uploads/portfolio/', blank=True),
        ),
    ]
