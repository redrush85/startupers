from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from main.feed import LatestNewsFeed

urlpatterns = [
    # Examples:
    url(r'^$', 'main.views.home', name='home'),
    url(r'^rss/$', LatestNewsFeed(), name='rss_feed'),
    url(r'^portfolio/$', 'main.views.portfolio', name='portfolio'),
    url(r'^portfolio/(?P<slug>[^\.]+).html', 'main.views.view_portfolio', name='view_portfolio'),
    url(r'^blog/$', 'main.views.blog', name='view_blog'),
    url(r'^about/$', 'main.views.about', name='about'),
    url(r'^contacts/$', 'main.views.contacts', name='contacts'),
    url(r'^faq/$', 'main.views.faq_page', name='faq'),
    url(r'^services/$', 'main.views.services', name='services'),
    url(r'^thanks/$', 'main.views.thanks', name='thanks'),
    url(r'^ajax_subscribe/$', 'main.views.ajax_subscribe'),
    url(r'^blog/(?P<category_slug>[^\.]+)/(?P<post_slug>[^\.]+).html', 'main.views.view_post', name='view_post'),
    url(r'^blog/(?P<category_slug>[^\.]+)/$', 'main.views.view_blog_category', name='view_blog_category'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^ckeditor/', include('ckeditor.urls')),

    url(r'^admin/', include(admin.site.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


